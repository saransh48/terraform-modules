resource "azurerm_resource_group" "rg" {
  name = var.rg_name
  location = var.rg_location
}

module "bastion_host" {
  source                               = "./modules/bastianHost"
  rg_location                          = var.rg_location
  rg_name                              = var.rg_name
  vnet_name                            = var.vnet_name
  vnet_address_space                   = var.vnet_address_space
  bastion_subnet_name                  = var.bastion_subnet_name
  bastion_subnet_address_prefixes      = var.bastion_subnet_address_prefixes
  public_ip_address_name               = var.public_ip_address_name
  private_ip_address_allocation_method = var.private_ip_address_allocation_method
  public_ip_address_sku                = var.public_ip_address_sku
  bastion_host_name                    = var.bastion_host_name
  bastian_ip_configuration_name        = var.bastian_ip_configuration_name
}
