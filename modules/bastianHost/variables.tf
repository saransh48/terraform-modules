variable "rg_name" {
  type        = string
  description = "Resource group name"
}

variable "rg_location" {
  type        = string
  description = "Resource group name"
}

variable "vnet_name" {
  type        = string
  description = "Virtual network name"
}

variable "vnet_address_space" {
  type        = list
  description = "virtual network address space"
}

variable "bastion_subnet_name" {
  type = string
  description = "Bastian Host Subnet name"
}

variable "bastion_subnet_address_prefixes" {
  type = list
  description = "Bastian Host Subnet name"
}

variable "public_ip_address_name" {
  type = string
  description = "Public IP address name"
}

variable "private_ip_address_allocation_method" {
  type = string
  description = "Private IP address allocation method"
}

variable "public_ip_address_sku" {
  type = string
  description = "Public IP address SKU"
}

variable "bastion_host_name" {
  type = string
  description = "Bastion host name"
}

variable "bastian_ip_configuration_name" {
  type = string
  description = "Bastion IP configuration name"
}
