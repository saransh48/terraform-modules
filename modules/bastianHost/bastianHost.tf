data "azurerm_resource_group" "rg" {
  name     = var.rg_name
}

resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  address_space       = var.vnet_address_space
  location            = var.rg_location
  resource_group_name = var.rg_name
}

resource "azurerm_subnet" "subnet" {
  name                 = var.bastion_subnet_name
  resource_group_name  = var.rg_name
  virtual_network_name = var.rg_name
  address_prefixes     = var.bastion_subnet_address_prefixes
}

resource "azurerm_public_ip" "public_ip" {
  name                = var.public_ip_address_name
  location            = var.rg_location
  resource_group_name = var.rg_name
  allocation_method   = var.private_ip_address_allocation_method
  sku                 = var.public_ip_address_sku
}

resource "azurerm_bastion_host" "bastion_host" {
  name                = var.bastion_host_name
  location            = var.rg_location
  resource_group_name = var.rg_name

  ip_configuration {
    name                 = var.bastian_ip_configuration_name
    subnet_id            = azurerm_subnet.subnet.id
    public_ip_address_id = azurerm_public_ip.public_ip.id
  }
}